<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

    // Route::domain('admin.multiple-account.local')->group(function(){
        Route::get('/admin/login','AuthAdmin\LoginController@showLoginForm')->name('admin.login');
        Route::post('/admin/login','AuthAdmin\LoginController@login')->name('admin.login.submit');
        Route::get('/admin/logout','AuthAdmin\LoginController@logout')->name('admin.logout');
        
        Route::get('/admin','AdminController@index')->name('admin.dashboard');   
    // });

    
 

